//
//  PhoneEnterViewController.swift
//  RoboCall
//
//  Created by ----- on 01/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit
import Toast_Swift
import Contacts
class PhoneEnterViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countrycodeTextField: UITextField!
    var contactStore = CNContactStore()
    var contacts = [ContactStruct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        contactStore.requestAccess(for: .contacts){ (success, error) in
            if success{
                print("Access")
            }
        }
        countrycodeTextField.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueAction(_ sender: Any) {
        guard(whitespaceValidation(countrycodeTextField.text!) == true)
            else
        {
            return self.view.makeToast("Please enter the country code")
        }
        guard(whitespaceValidation(phoneTextField.text!) == true)
            else
        {
            return self.view.makeToast("Please enter the phone number")
        }
        guard(validate(value: phoneTextField.text!) == true)
            else
        {
            return self.view.makeToast("Please enter phone number atleast 7 digits")
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VerifyCodeViewController") as! VerifyCodeViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[6-9][0-9]{9}$";
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    func whitespaceValidation(_ name:String) -> Bool
    {
        let whitespaceSet = CharacterSet.whitespaces
        if !name.trimmingCharacters(in: whitespaceSet).isEmpty
        {
            return true
        }
        else
        {
            return false
        }
    }
    @IBAction func countryEndEdit(_ sender: Any) {
         phoneTextField.becomeFirstResponder()
    }
    
    @IBAction func phoneEndEdit(_ sender: Any) {
         phoneTextField.resignFirstResponder()
    }
    
}

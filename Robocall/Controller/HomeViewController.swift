//
//  HomeViewController.swift
//  RoboCall
//
//  Created by ---- on 30/04/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
class HomeViewController: UIViewController {
    
    @IBOutlet weak var segmentView: UIView!
    var segmentindex = Int()
    var menu_Vc : RCContainerViewController!
    var segmentedViewController = SJSegmentedViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        menu_Vc = self.storyboard?.instantiateViewController(withIdentifier: "RCContainerViewController")as! RCContainerViewController
        segmentedViewController.delegate = self
        navigationController?.isNavigationBarHidden = true
        //        segmentedViewController.segmentTitleColor = UIColor.black
        //        segmentedViewController.segmentTitleFont = UIFont.systemFont(ofSize: 12.0)
        segmentedViewController.selectedSegmentViewHeight = 3.0
        segmentedViewController.segmentViewHeight = 40
        if let storyboard = self.storyboard{
            let headerController = storyboard.instantiateViewController(withIdentifier: "RCCallLogViewController")as! RCCallLogViewController
            //headerController.title = "CALL LOG"
            let view1 = UIImageView()
            view1.frame.size.width = 10
            view1.image = UIImage(named: "call")
            view1.contentMode = .scaleAspectFit
            view1.backgroundColor = UIColor(red: 24/255.0, green: 65/255.0, blue: 219/255.0, alpha: 0.85)
            headerController.navigationItem.titleView = view1
            let firstViewController = storyboard.instantiateViewController(withIdentifier: "RCContactsViewController")as! RCContactsViewController
            //firstViewController.title = "CONTACTS"
            let view2 = UIImageView()
            view2.frame.size.width = 10
            view2.image = UIImage(named: "contacts")
            view2.contentMode = .scaleAspectFit
            view2.backgroundColor = UIColor(red: 24/255.0, green: 65/255.0, blue: 219/255.0, alpha: 0.85)
            firstViewController.navigationItem.titleView = view2
            segmentedViewController.segmentTitleColor = UIColor.white
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "RCFavouriteViewController")as! RCFavouriteViewController
            //secondViewController.title = "FAVOURITE"
            let view3 = UIImageView()
            view3.frame.size.width = 10
            view3.image = UIImage(named: "fav")
            view3.contentMode = .scaleAspectFit
            view3.backgroundColor = UIColor(red: 24/255.0, green: 65/255.0, blue: 219/255.0, alpha: 0.85)
            secondViewController.navigationItem.titleView = view3
            let thirdViewController = storyboard.instantiateViewController(withIdentifier: "RCBlockedViewController")as! RCBlockedViewController
            // thirdViewController.title = "BLOCK"
            let view4 = UIImageView()
            view4.frame.size.width = 10
            view4.image = UIImage(named: "block")
            view4.contentMode = .scaleAspectFit
            view4.backgroundColor = UIColor(red: 24/255.0, green: 65/255.0, blue: 219/255.0, alpha: 0.85)
            thirdViewController.navigationItem.titleView = view4
            
            segmentedViewController.segmentShadow = SJShadow.dark()
            //segmentedViewController.segmentBounces = false
            segmentedViewController.selectedSegmentViewColor = UIColor.white
            segmentedViewController.segmentControllers = [headerController,firstViewController,secondViewController,thirdViewController]
            segmentedViewController.segmentBackgroundColor = UIColor(red: 4/255.0, green: 51/255.0, blue: 255/255.0, alpha: 0.85)
            
            addChildViewController(segmentedViewController)
            self.segmentView.addSubview(segmentedViewController.view)
            segmentedViewController.view.frame = self.segmentView.bounds
            
            
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
            swipeLeft.direction = .left
            self.view.addGestureRecognizer(swipeLeft)
            
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
            swipeRight.direction = .right
            self.view.addGestureRecognizer(swipeRight)
            
            
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
            show_menu()
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
            close_on_swipe()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        if AppDelegate.menu_Bool{
            show_menu()
        }else{
            close_menu()
        }
    }
    func close_on_swipe(){
        if AppDelegate.menu_Bool{
            show_menu()
        }else{
            close_menu()
        }
    }
    func show_menu(){
        UIView.animate(withDuration: 0.3, animations:{ ()->Void in
            self.menu_Vc.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.menu_Vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.addChildViewController(self.menu_Vc)
            self.view.addSubview(self.menu_Vc.view)
            self.menu_Vc.navigationController?.isNavigationBarHidden = true
            AppDelegate.menu_Bool = false
        })
    }
    func close_menu(){
        UIView.animate(withDuration: 0.3, animations:{ ()->Void in
            self.menu_Vc.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) {(finished)in
            
            self.menu_Vc.view.removeFromSuperview()
        }
        AppDelegate.menu_Bool = true
    }
    
}
extension HomeViewController: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        if segmentedViewController.segments.count > 0 {
            
            _ = segmentedViewController.segments[index]
            print(index)
            segmentindex = index
        }
    }
}


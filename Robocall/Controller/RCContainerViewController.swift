//
//  RCContainerViewController.swift
//  RoboCall
//
//  Created by ----- on 01/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit

class RCContainerViewController: UIViewController {
var sideMenuOpen = false
    override func viewDidLoad() {
        super.viewDidLoad()
NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("toggleSideMenu"), object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func toggleSideMenu(){
        if sideMenuOpen{
            sideMenuOpen = false
            
        }else{
            
        }
        UIView.animate(withDuration: 0.3){
            self.view.layoutIfNeeded()
        }
    }
}

//
//  VerifyCodeViewController.swift
//  RoboCall
//
//  Created by ------ on 01/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit

class VerifyCodeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var verfyFourText: UITextField!
    @IBOutlet weak var verfyThreeText: UITextField!
    @IBOutlet weak var verfyTwoText: UITextField!
    @IBOutlet weak var verfyOneText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        verfyOneText.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        verfyTwoText.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        verfyThreeText.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        verfyFourText.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        verfyOneText.becomeFirstResponder()
        
    }
    @objc func textFieldDidChange(textField:UITextField){
        let text = textField.text
        if text?.utf16.count == 1{
            switch textField{
            case verfyOneText:
                verfyTwoText.becomeFirstResponder()
            case verfyTwoText:
                verfyThreeText.becomeFirstResponder()
            case verfyThreeText:
                verfyFourText.becomeFirstResponder()
            case verfyFourText:
                verfyFourText.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func verifyAction(_ sender: Any) {
        if verfyOneText.text == "" && verfyTwoText.text == "" && verfyThreeText.text == "" && verfyFourText.text == ""{
            self.view.makeToast("Please enter the Verification code")
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    func whitespaceValidation(_ name:String) -> Bool
    {
        let whitespaceSet = CharacterSet.whitespaces
        if !name.trimmingCharacters(in: whitespaceSet).isEmpty
        {
            return true
        }
        else
        {
            return false
        }
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField.text!.count < 1  && string.count > 0{
//            let nextTag = textField.tag + 1
//
//            // get next responder
//            var nextResponder = textField.superview?.viewWithTag(nextTag)
//
//            if (nextResponder == nil){
//
//                nextResponder = textField.superview?.viewWithTag(1)
//            }
//            textField.text = string
//            nextResponder?.becomeFirstResponder()
//            return false
//        }
//        else if textField.text!.count >= 1  && string.count == 0{
//            // on deleting value from Textfield
//            let previousTag = textField.tag - 1
//
//            // get next responder
//            var previousResponder = textField.superview?.viewWithTag(previousTag)
//
//            if (previousResponder == nil){
//                previousResponder = textField.superview?.viewWithTag(1)
//            }
//            textField.text = ""
//            previousResponder?.becomeFirstResponder()
//            return false
//        }
//        return true
//
//    }
}

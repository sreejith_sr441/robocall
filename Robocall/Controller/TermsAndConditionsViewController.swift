//
//  TermsAndConditionsViewController.swift
//  RoboCall
//
//  Created by ----- on 01/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func nextAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PhoneEnterViewController") as! PhoneEnterViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

}

//
//  RCSideMenuTableViewController.swift
//  RoboCall
//
//  Created by ----- on 01/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit

class RCSideMenuTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row == 2{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContactListingViewController") as! ContactListingViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if indexPath.row == 5{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
}

//
//  ContactListingViewController.swift
//  RoboCall
//
//  Created by ----- on 03/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit
import Contacts
class ContactListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var contactStore = CNContactStore()
    var contacts = [ContactStruct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchContact()
    }
    func fetchContact(){
        
     let key = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey]as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try! contactStore.enumerateContacts(with: request){(contact , stopingPointer) in
            let name = contact.givenName
            let familyName = contact.familyName
            let number = contact.phoneNumbers.first?.value.stringValue
            let contactToAppend = ContactStruct(givenName: name, familName: familyName, number: number!)
            self.contacts.append(contactToAppend)
        }
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListTableViewCell", for: indexPath) as! ContactListTableViewCell
        let contactDisplay = contacts[indexPath.row]
        cell.phoneOrNameLabel.text = contactDisplay.givenName + " " + contactDisplay.familName
        cell.numebLabel.text = contactDisplay.number
        return cell
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        AppDelegate.menu_Bool = false
    }
    
    

}

//
//  ContactStruct.swift
//  RoboCall
//
//  Created by ----- on 03/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import Foundation
struct ContactStruct {
    let givenName : String
    let familName : String
    let number : String
}

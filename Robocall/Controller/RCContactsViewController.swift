//
//  RCContactsViewController.swift
//  RoboCall
//
//  Created by ---- on 01/05/19.
//  Copyright © 2019 Xtapps. All rights reserved.
//

import UIKit
import Contacts
class RCContactsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var contactStore = CNContactStore()
    var contacts = [ContactStruct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchContact()

        // Do any additional setup after loading the view.
    }

    func fetchContact(){
        
        let key = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey]as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try! contactStore.enumerateContacts(with: request){(contact , stopingPointer) in
            let name = contact.givenName
            let familyName = contact.familyName
            let number = contact.phoneNumbers.first?.value.stringValue
            let contactToAppend = ContactStruct(givenName: name, familName: familyName, number: number!)
            self.contacts.append(contactToAppend)
        }
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RCContactTableViewCell", for: indexPath) as! RCContactTableViewCell
        let contactDisplay = contacts[indexPath.row]
        cell.phoneOrNumberLabel.text = contactDisplay.givenName + " " + contactDisplay.familName
        cell.numberLabel.text = contactDisplay.number
        return cell
    }
    


}
